import { useState, useEffect } from "react";
import List from "../src/components/List/List";

function MainPage() {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(`/products.json`);
      const info = await response.json();
      setData(info);
    };

    fetchData();
  }, []);
  return (
    <>
      <div className="page-div">
        <h1 className="page-div__title">MainPage</h1>
      </div>
      <List data={data} />
    </>
  );
}

export default MainPage;
