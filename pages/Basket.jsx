import Card from "../src/components/Card/Card";

function Basket() {
  let cartItems = JSON.parse(localStorage.getItem("Basket")) || [];
  return (
    <>
      <div className="page-div">
        <h1 className="page-div__title">Basket page</h1>
      </div>

      <ul className="list">
        {cartItems.map((item, index) => (
          <Card card={item} key={index} />
        ))}
      </ul>
    </>
  );
}

export default Basket;
