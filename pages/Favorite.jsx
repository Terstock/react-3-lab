import Card from "../src/components/Card/Card";

function Favorite() {
  let favItems = JSON.parse(localStorage.getItem("Favorite")) || [];
  return (
    <>
      <div className="page-div">
        <h1 className="page-div__title">Favorite page</h1>
      </div>
      <ul className="list">
        {favItems.map((item, index) => (
          <Card card={item} key={index} />
        ))}
      </ul>
    </>
  );
}

export default Favorite;
