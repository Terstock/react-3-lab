import Card from "../Card/Card";
import "./List.scss";

function List({ data }) {
  return (
    <ul className="list">
      {data.map((item, index) => (
        <Card card={item} key={index} />
      ))}
    </ul>
  );
}

export default List;
