import { createBrowserRouter } from "react-router-dom";
import Basket from "../../pages/Basket";
import Favorite from "../../pages/Favorite";
import MainPage from "../../pages/MainPage";
import Layout from "./Layout/Layout";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        index: true,
        element: <MainPage />,
      },
      {
        path: "/basket",
        element: <Basket />,
      },
      {
        path: "/favorite",
        element: <Favorite />,
      },
    ],
  },
]);
