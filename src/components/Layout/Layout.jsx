import { Link } from "react-router-dom";
import { Outlet } from "react-router-dom";
import { useState } from "react";
import basketIcon from "../assets/shopping-basket.svg";
import basicFavorite from "../assets/favorite-false.svg";
import "./Layout.scss";
import "../reset.scss";

function Layout() {
  const [basketProducts, setBasketProducts] = useState(() => {
    let basketProducts = JSON.parse(localStorage.getItem("Basket")) || [];
    return basketProducts.length;
  });
  const [favProducts, setFavProducts] = useState(() => {
    const favProducts = JSON.parse(localStorage.getItem("Favorite")) || [];
    return favProducts.length;
  });

  return (
    <>
      <header className="header">
        <h1>Header Logo</h1>
        <div className="header__div">
          <img src={basketIcon} className="header__photo" alt="basket" />
          <p className="header__text">{basketProducts}</p>
          <img src={basicFavorite} className="header__photo" alt="favorite" />
          <p className="header__text">{favProducts}</p>
        </div>
      </header>
      <nav className="nav">
        <Link to="/" className="nav__link">
          Main Page
        </Link>
        <Link to="/basket" className="nav__link">
          Basket
        </Link>
        <Link to="/favorite" className="nav__link">
          Favorite
        </Link>
      </nav>

      <Outlet context={{ setBasketProducts, setFavProducts }} />
    </>
  );
}

export default Layout;
