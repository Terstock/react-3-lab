import Button from "../Button/Button";
import "../Card/Card.scss";
import "../Button/Button.scss";
import "../Modal/Modal.scss";
import Modal from "../Modal/Modal";
import ModalWrapper from "../Modal/ModalWrapper";
import ModalHeader from "../Modal/ModalHeader";
import ModalClose from "../Modal/ModalClose";
import ModalBody from "../Modal/ModalBody";
import ModalFooter from "../Modal/ModalFooter";
import ModalImage from "../Modal/ModalImage";
import ModalText from "../Modal/ModalText";
import falseFavorite from "../assets/favorite-false.svg";
import trueFavorite from "../assets/favorite-true.svg";
import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import "./Card.scss";
import { useOutletContext } from "react-router-dom";

function Card({ card }) {
  const [isVisible, setIsVisible] = useState(false);
  const [modalType, setModalType] = useState(null);
  const [isFavorite, setIsFavorite] = useState(false);
  const [inStorage, setInStorage] = useState([]);
  const { setBasketProducts, setFavProducts } = useOutletContext();

  function changeModalState(type) {
    setIsVisible(!isVisible);
    setModalType(type);
  }

  function handleWrapperClick(e) {
    e.stopPropagation();
  }

  function toLocalStorage(card) {
    setInStorage(!inStorage);
    const storedItems = JSON.parse(localStorage.getItem("Basket")) || [];
    const buyProduct = storedItems.find(
      (item) => item.vendor_code === card.vendor_code
    );
    if (!buyProduct) {
      storedItems.push(card);
      setBasketProducts(storedItems.length);
      localStorage.setItem("Basket", JSON.stringify(storedItems));
    }
  }

  function deleteProduct(card) {
    const storedItems = JSON.parse(localStorage.getItem("Basket")) || [];
    const updatedProducts = storedItems.filter(
      (item) => item.vendor_code !== card.vendor_code
    );

    setBasketProducts(updatedProducts.length);
    localStorage.setItem("Basket", JSON.stringify(updatedProducts));
    window.location.reload();
  }

  function handleFavorite(card) {
    setIsFavorite(!isFavorite);

    const favoriteItems = JSON.parse(localStorage.getItem("Favorite")) || [];

    const favCard = favoriteItems.find(
      (item) => item.vendor_code === card.vendor_code
    );

    if (isFavorite === false) {
      if (!favCard) {
        favoriteItems.push(card);
        setFavProducts(favoriteItems.length);
        localStorage.setItem("Favorite", JSON.stringify(favoriteItems));
      }
    } else if (isFavorite === true) {
      if (favCard) {
        const updatedFavorites = favoriteItems.filter(
          (item) => item.vendor_code !== card.vendor_code
        );
        setFavProducts(updatedFavorites.length);
        localStorage.setItem("Favorite", JSON.stringify(updatedFavorites));
        window.location.reload();
      }
    }
  }

  useEffect(() => {
    const savedFavoriteState = JSON.parse(
      localStorage.getItem(`photoState-${card.vendor_code}`)
    );
    if (savedFavoriteState !== null) {
      setIsFavorite(savedFavoriteState);
    }
  }, [card.vendor_code]);

  useEffect(() => {
    localStorage.setItem(
      `photoState-${card.vendor_code}`,
      JSON.stringify(isFavorite)
    );
  }, [isFavorite, card.vendor_code]);

  const handleFavoriteToggle = () => {
    setIsFavorite((isFavorite) => !isFavorite);
  };

  useEffect(() => {
    if (isVisible === true) {
      document.body.classList.add("no-scroll");
    } else {
      document.body.classList.remove("no-scroll");
    }
  }, [isVisible]);

  return (
    <>
      <li className="card">
        <button
          className="delete-btn"
          title="Delete Product"
          onClick={() => changeModalState("delete")}
        >
          ✖
        </button>
        <img className="card__photo" src={card.path} alt="" />
        <button
          className="card__button"
          onClick={() => {
            handleFavorite(card), handleFavoriteToggle;
          }}
        >
          <img
            className="card__button-img"
            src={isFavorite ? trueFavorite : falseFavorite}
            alt=""
          />
        </button>
        <div className="card__info">
          <h1 className="card__title">{card.name}</h1>
          <h2 className="card__price">${card.price}</h2>
          <h3 className="card__color">Game disk color: {card.color}</h3>
          <h3 className="card__vendor-code">№{card.vendor_code}</h3>

          <div className="app-div">
            <Button
              type="button"
              onClick={() => changeModalState("delete")}
              className="new__button new__button--purple"
            >
              Delete product
            </Button>
            <Button
              type="button"
              onClick={() => changeModalState("add")}
              className="new__button new__button--white"
            >
              Add to basket
            </Button>
          </div>
        </div>
      </li>
      {isVisible ? (
        <ModalWrapper className="modal__wrapper" onClick={changeModalState}>
          <Modal className="modal" onClick={handleWrapperClick}>
            <ModalHeader className="modal__header">
              <ModalClose
                className="modal__close-btn"
                onClick={changeModalState}
              ></ModalClose>
            </ModalHeader>
            <ModalBody className="modal__body">
              {modalType === "delete" ? (
                <>
                  <ModalImage
                    className="modal__body-rectangle"
                    image={card.path}
                  />
                  <ModalText className="modal__body-title">
                    Product Delete!
                  </ModalText>
                  <ModalText className="modal__body-text">
                    By clicking the “Yes, Delete” button, {card.name} will be
                    deleted.
                  </ModalText>
                </>
              ) : (
                <>
                  <ModalText className="modal__body-title">
                    Add Product {card.name}
                  </ModalText>
                  <ModalText className="modal__body-text">
                    Description for you product
                  </ModalText>
                </>
              )}
            </ModalBody>
            {modalType === "delete" ? (
              <ModalFooter
                firstText="NO, CANCEL"
                secondaryText="YES, DELETE"
                firstClick={changeModalState}
                secondaryClick={() => {
                  changeModalState, deleteProduct(card);
                }}
              ></ModalFooter>
            ) : (
              <ModalFooter
                firstText="ADD TO BASKET"
                firstClick={() => {
                  changeModalState, toLocalStorage(card);
                }}
              ></ModalFooter>
            )}
          </Modal>
        </ModalWrapper>
      ) : null}
    </>
  );
}

Card.propTypes = {
  card: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    path: PropTypes.string.isRequired,
    vendor_code: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
  }).isRequired,
};

// Card.defaultProps = {
//   card: {
//     path: "./public/photos/unavaible.jpg",
//     color: "unknown",
//   },
//
// };

export default Card;
